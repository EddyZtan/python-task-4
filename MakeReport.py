import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pandas_datareader as web

df = pd.read_csv('SP500.csv', index_col='Date')
df_SP = pd.read_excel('StockPrices.xlsx', index_col='Date')
df_SP_temp = pd.read_excel('StockPrices.xlsx', index_col='Date')

print(df_SP['A'].resample('M').ffill().pct_change().head())

df_SP_temp['A'] = df_SP['A'].resample('M').ffill().pct_change()

print(df_SP_temp.head())

